package A1;// Attributes, methods and even constructors possible
// 1. See decompiled Enum class for details
// 2. See javadoc on Enum.class

//class A1.Seasons extends Enum
enum Seasons
{
	SPRING, SUMMER, AUTUMN, WINTER;

	// here it's possible to return an user friendly name (in contrast to name())
	public String toString()
	{
		return super.toString();
	}

	public boolean acceptable()
	{
		return (this == SPRING || this == SUMMER);
	}

	/**
	 * 
	 * @param args
	 */
	public static void main( String[] args )
	{
		Seasons s = SUMMER;
		Seasons a = valueOf("AUTUMN");
		
		System.out.println( s + " less than " + a + ": " + s.compareTo( a ) );
		
		Seasons sn[] = values();
		for( Seasons j : sn )
			System.out.println( j + " == " + j.ordinal() );
		
		// enums can be used in "switch"-statements
		switch( s )
		{
		case SPRING:
			System.out.println( "March-May" );
			break;
		case SUMMER:
			System.out.println( "June-August" );
			break;
		case AUTUMN:
			System.out.println( "September-November" );
			break;
		case WINTER:
			System.out.println( "December-February" );
			break;
		default:
			assert false;
		}
		System.out.println( s + " acceptable: " + s.acceptable() );
		System.out.println( a + " acceptable: " + a.acceptable() );
	}
}