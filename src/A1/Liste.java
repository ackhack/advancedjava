package A1;

/**
 * @author Laumeyer
 * <p>
 * Simple (single) linked list. New elements will be added at the head
 * of the list.
 */
class Liste {
    private static class ListElem {
        int data;
        ListElem next;

        ListElem(int data, ListElem next) {
            this.data = data;
            this.next = next;
        }
    }

    private ListElem header;

    public void add(int data) {
        if (header == null)
            header = new ListElem(data, null);
        else {
            ListElem nle = new ListElem(data, header);
            header = nle;
        }
    }

    public int contains(int data) {
    	int ret = -1;
        ListElem x = header;
        do {
        	ret++;
            if (x.data == data) {
                return ret;
            } else {
                x = x.next;
            }
        } while (x.next != null);
        return -1;
    }

    public boolean delete(int data) {
    	ListElem elem = header;
    	int pos= this.contains(data);
    	int i = 0;
		if (pos != -1) {
			while (i < pos-1) {
				i++;
				elem = elem.next;
			}
			elem.next = elem.next.next;
			return true;
		}
        return false;
    }


    public static void main(String[] args) {
        // What happens here (picture)?
        Liste l = new Liste();
        l.add(7);
        l.add(5);
        l.add(12);
        l.add(44);
        System.out.println(l.contains(5));
		System.out.println(l.delete(5));
		System.out.println(l.contains(5));
        System.out.println("Ready ;-)");
    }
}