package A1;

import java.util.Iterator;

@SuppressWarnings({"unchecked"})
public class ListeIterable<T> implements Iterable	// 1. javadoc --> Iterable
{
	class ListElem
	{
		T data;
		ListElem next;

		ListElem( T data, ListElem next )
		{
			this.data = data;
			this.next = next;
		}
	}

	ListElem header;
	int Listsize = 0;

	public void add( T data )
	{
		Listsize++;
		if( header == null )
			header = new ListElem( data, null );
		else
		{
			header = new ListElem( data, header );
		}
	}

	int size() {
		return Listsize;
	}

	T get(int index) throws NullPointerException{

		ListElem le = header;

		for(int x = 0;x!=index;x++) {
			le = le.next;
		}

		return le.data;
	}

	boolean contains(T value) {

		ListElem le = header;

		while (le.data != value) {
			if (le.next == null) {
				return false;
			}
			le = le.next;
		}

		return true;
	}

	void remove(int index) {

		ListElem le = header;

		Listsize--;

		if (index == 0) {
			header = header.next;
		}

		for(int x = 0;x<index-1;x++) {
			le = le.next;
		}
		le.next = le.next.next;

	}

	// contract for interface Iterable
	public Iterator iterator()   // 2. javadox --> Iterator
	{
		return new Iterator()
		{
			private ListElem iter = header;

			public boolean hasNext()
			{
				return iter != null;
			}

			public Object next()
			{
				T ret = iter.data;
				iter = iter.next;
				return ret;
			}
		};
	}

	public static void main( String[] args )
	{
		ListeIterable l = new ListeIterable();
		l.add( 7 );
		l.add( 5 );
		l.add( 12 );
		l.add( 44 );
		for( Object i : l )
			System.out.println( i );
		System.out.println(l.size());
		System.out.println(l.get(1));
	}
}