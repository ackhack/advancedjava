package A1;

enum Monate {
    JANUARY(31), FEBRUARY(29), MARCH(31), APRIL(30),
    MAY(31), JUNE(30), JULY(31), AUGUST(31),
    SEPTEMBER(30), OCTOBER(31), NOVEMBER(30), DECEMBER(31);
    int days;

    Monate(int days) {
        this.days = days;
    }

    public static void main(String[] args) {
        Monate s = SEPTEMBER;
        int days = s.days;
        Seasons seasons;
        switch (s) {
            case DECEMBER:
            case JANUARY:
            case FEBRUARY:
                seasons = Seasons.WINTER;
                break;
            case MARCH:
            case MAY:
            case APRIL:
                seasons = Seasons.SPRING;
                break;
            case JUNE:
            case JULY:
            case AUGUST:
                seasons = Seasons.SUMMER;
                break;
            case SEPTEMBER:
            case NOVEMBER:
            case OCTOBER:
                seasons = Seasons.AUTUMN;
                break;
            default:
                seasons = Seasons.SUMMER;
                break;
        }
        System.out.println(days);
        System.out.println(seasons.toString());
    }
}
