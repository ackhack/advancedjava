package A1;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ListIterableTest {

    private static ListeIterable<String> list = new ListeIterable<>();

    @BeforeClass
    public static void setUpBeforeClass()
    {
        System.out.println("setUpBeforeClass");
        list.add("Number 1");
        list.add("Number 2");
        list.add("Number 3");
        list.add("Number 4");
    }

    @AfterClass
    public static void tearDownAfterClass()
    {
        System.out.println("tearDownAfterClass");
    }

    @Test
    public void Testadd() {
        assertEquals(4,list.size());
    }

    @Test
    public void Testget() {
        assertEquals("Number 3", list.get(1));
    }

    @Test
    public void Testcontains() {
        assertTrue(list.contains("Number 1"));
        assertFalse(list.contains("Number 0"));
    }

    @Test
    public void Testremove() {
        assertTrue(list.contains("Number 4"));
        list.remove(0);
        assertEquals(3,list.size());
        assertFalse(list.contains("Number 4"));
    }

}
