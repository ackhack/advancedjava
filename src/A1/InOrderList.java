package A1;

public class InOrderList<T> extends ListeIterable<T> {

    @Override
    public void add(T data) {
        Listsize++;
        if( header == null ) {
            header = new ListElem(data, null);
        }
        else
        {
            ListElem le = header;
            while (le.next != null) {
                le = le.next;
            }
            le.next = new ListElem(data, null);
        }
    }
}
