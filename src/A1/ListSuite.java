package A1;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        ListIterableTest.class,
        InOrderListTest.class,
        })
public class ListSuite
{
}