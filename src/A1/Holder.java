package A1;

public class Holder {

    private static double getNumber() {
        try {
            Thread.sleep(10000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("was here");
        return Math.random();
    }
    private static class Holder1 {
            static final double x = getNumber();
    }

    private void test() {
        System.out.println("here");
        System.out.println(Holder1.x);
    }

    public static void main(String[] args) {
        new Holder().test();
    }
}
