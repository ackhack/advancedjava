package A1;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class InOrderListTest {

    private static InOrderList<String> list = new InOrderList<>();

    @BeforeClass
    public static void setUpBeforeClass()
    {
        System.out.println("setUpBeforeClass");
        list.add("Number 1");
        list.add("Number 2");
        list.add("Number 3");
        list.add("Number 4");
    }

    @AfterClass
    public static void tearDownAfterClass()
    {
        System.out.println("tearDownAfterClass");
    }

    @Test
    public void Testadd() {
        assertEquals(4,list.size());
    }

    @Test
    public void Testget() {
        assertEquals("Number 3", list.get(2));
        assertEquals("Number 1", list.get(0));
        assertEquals("Number 4", list.get(3));
    }

    @Test
    public void Testcontains() {
        assertTrue(list.contains("Number 1"));
        assertTrue(list.contains("Number 4"));
        assertFalse(list.contains("Number 0"));
    }

    @Test
    public void Testremove() {
        assertTrue(list.contains("Number 2"));
        list.remove(1);
        assertEquals(3,list.size());
        assertFalse(list.contains("Number 2"));
        list.remove(0);
        assertFalse(list.contains("Number 1"));
        list.remove(1);
        assertEquals(1,list.size());
        assertFalse(list.contains("Number 4"));
    }
}
