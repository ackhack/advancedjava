package A2;

import java.util.Iterator;
import java.util.Stack;

public class Tree2<T> implements Iterable {

    private Knot<T> WurzelKnoten = new Knot<>(null);

    static class Knot<T> {
        Knot<T> Left;
        Knot<T> Right;
        T content;

        Knot(T val) {
            content = val;
        }
    }

    private void add(T t) {

        Knot<T> aktuell = WurzelKnoten;

        if (aktuell.content == null) {
            aktuell.content = t;
            return;
        }

        while (true) {
            if (Math.random() > 0.5) {
                if (aktuell.Left == null) {
                    aktuell.Left = new Knot<>(t);
                    return;
                } else {
                    aktuell = aktuell.Left;
                }
            } else {
                if (aktuell.Right == null) {
                    aktuell.Right = new Knot<>(t);
                    return;
                } else {
                    aktuell = aktuell.Right;
                }
            }
        }
    }

    @Override
    public Iterator<T> iterator()
    {
        return new Iterator<>()
        {

            private Stack<Knot<T>> s = new Stack<>();

            {
                if (WurzelKnoten != null)
                    s.push(WurzelKnoten);
            }

            public boolean hasNext()
            {
                return !s.empty();
            }

            public T next()
            {
                Knot<T> n = s.pop();

                if (n.Left != null)
                    s.push(n.Left);
                if (n.Right != null)
                    s.push(n.Right);

                return n.content;
            }
        };
    }

    public static void main(String[] args) {
        Tree2<String> BAUM = new Tree2<>();
        for (int i = 0; i < 10; i++) {
            BAUM.add(String.valueOf(i));
        }
        for (Object x : BAUM) {
            System.out.println(x.toString());
        }
    }
}