package A2;

import java.util.Iterator;
import java.util.Stack;

public class Tree implements Iterable {

    static class Knot {
        Knot Left;
        Knot Right;
        String content;

        public String toString() {
            return content;
        }
    }

    private Knot WurzelKnoten = new Knot();
    private Stack<Knot> stack = new Stack<>();

    private void add(Knot knot) {

        stack.push(knot);
        double r = Math.random();
        Knot aktuell = WurzelKnoten;

        while (true) {
            if (r > 0.5) {
                if (aktuell.Left == null) {
                    aktuell.Left = knot;
                    return;
                } else {
                    aktuell = aktuell.Left;
                }
            } else {
                if (aktuell.Right == null) {
                    aktuell.Right = knot;
                    return;
                } else {
                    aktuell = aktuell.Right;
                }
            }
        }
    }

    @Override
    public Iterator<Knot> iterator() {
        return stack.iterator();
    }

    public static void main(String[] args) {
        Tree BAUM = new Tree();
        for (int i = 0; i < 10; i++) {
            Knot x = new Knot();
            x.content = String.valueOf(i);
            BAUM.add(x);
        }
        for (Object x: BAUM) {
            System.out.println(x.toString());
        }
    }
}