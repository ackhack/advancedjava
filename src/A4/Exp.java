package A4;

@SuppressWarnings({"unused"})
public class Exp extends Function{

    {
        function = "exp(x)";
    }

    @Override
    public String getFunction() {
        return function;
    }

    @Override
    public double getValueat(double value) {
        return Math.exp(value);
    }

    @Override
    public String getDerivation() {
        return function;
    }

    @Override
    public double getDerivationValueat(double value) {
        return Math.exp(value);
    }
}
