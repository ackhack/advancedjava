package A4;

@SuppressWarnings({"unused"})
public abstract class Function {

    String function;

    public abstract String getFunction();

    public abstract double getValueat(double value);

    public abstract String getDerivation();

    public abstract double getDerivationValueat(double value);
}