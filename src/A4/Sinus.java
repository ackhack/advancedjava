package A4;

@SuppressWarnings({"unused"})
public class Sinus extends Function {

    {
        function = "sin(x)";
    }

    @Override
    public String getFunction() {
        return function;
    }

    @Override
    public double getValueat(double value) {
        return Math.sin(value);
    }

    @Override
    public String getDerivation() {
        return "cos(x)";
    }

    @Override
    public double getDerivationValueat(double value) {
        return Math.cos(value);
    }
}