package A5;

public final class ComplexNoPattern {
    private final double real;
    private final double im;

    ComplexNoPattern(double real, double im) {
        this.real = real;
        this.im = im;
    }

    ComplexNoPattern negate() {
        return new ComplexNoPattern(-real,-im);
    }

    ComplexNoPattern add(ComplexNoPattern x) {
        return new ComplexNoPattern(real+x.real, im+x.im);
    }

    ComplexNoPattern sub(ComplexNoPattern x) {
        return new ComplexNoPattern(real-x.real, im-x.im);
    }

    ComplexNoPattern mul(ComplexNoPattern x) {
        double treal = real * x.real - im * x.im;
        double tim = real * x.im + x.real * im;
        return new ComplexNoPattern(treal,tim);
    }

    @SuppressWarnings("EqualsWhichDoesntCheckParameterClass")
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        ComplexNoPattern x = (ComplexNoPattern) obj;
        return x.real == this.real && x.im == im;
    }

    @Override
    public int hashCode() {
        return (int) (real*123456789 + im);
    }
}
