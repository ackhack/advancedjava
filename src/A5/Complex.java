package A5;

import java.io.*;
import java.util.HashMap;


public class Complex implements Comparable<Complex> {

    private double real;
    private double im;
    private static HashMap<Integer, Complex> hmap = new HashMap<>();

    private Complex(double real, double im) {
        this.real = real;
        this.im = im;
    }

    Complex negate() {
        return Complex.valueOf(-real, -im);
    }

    Complex add(Complex x) {
        return Complex.valueOf(real + x.real, im + x.im);
    }

    Complex sub(Complex x) {
        return Complex.valueOf(real - x.real, im - x.im);
    }

    Complex mul(Complex x) {
        double treal = real * x.real - im * x.im;
        double tim = real * x.im + x.real * im;
        return Complex.valueOf(treal, tim);
    }

    private double getAbsolute() {
        return real * real + im * im;
    }

    @SuppressWarnings("EqualsWhichDoesntCheckParameterClass")
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        Complex x = (Complex) obj;
        return x.real == this.real && x.im == im;
    }

    @Override
    public int hashCode() {
        return (int) (real * 123456789 + im);
    }

    static Complex valueOf(double real, double im) {

        Complex x = new Complex(real, im);

        if (!hmap.containsValue(x))
            hmap.put(x.hashCode(), x);
        return hmap.get(x.hashCode());
    }

    @Override
    public int compareTo(Complex c) {
        return Double.compare(this.getAbsolute(), c.getAbsolute());
    }

    private void writeObject(String path) throws IOException {

        ObjectOutputStream stream = new ObjectOutputStream(new FileOutputStream(new File(path)));
        stream.writeDouble(real);
        stream.writeDouble(im);
        stream.close();
    }

    private static Complex readObject(String path) throws IOException {

        try (ObjectInputStream stream = new ObjectInputStream(new FileInputStream(new File(path)))) {
            return Complex.valueOf(stream.readDouble(), stream.readDouble());
        }
    }

    public static void main(String[] args) throws IOException {
        Complex c = Complex.valueOf(1,1);
        c.writeObject("Complex.ser");
        Complex x = Complex.readObject("Complex.ser");
        System.out.println(x.im + " " + c.im);
        System.out.println(x.real + " " + c.real);
        if (x == c) {
            System.out.println(true);
        } else {
            System.out.println(false);
        }
    }


}