package A5;

import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.jupiter.api.Assertions.*;

public class ComplexNoPatternTest {

    private static ComplexNoPattern com;

    @BeforeClass
    public static void setup() {
        com = new ComplexNoPattern(10,5);
    }

    @Test
    public void testneg() {
        assertEquals(new ComplexNoPattern(-10,-5), com.negate());
    }

    @Test
    public void testadd() {
        assertEquals(new ComplexNoPattern(20,7),com.add(new ComplexNoPattern(10,2)));
    }

    @Test
    public void testsub() {
        assertEquals(new ComplexNoPattern(0,3),com.sub(new ComplexNoPattern(10,2)));
        assertNotEquals(new ComplexNoPattern(10,3),com.sub(new ComplexNoPattern(1020,2450)));
    }

    @Test
    public void testmul() {
        assertEquals(new ComplexNoPattern(90,70),com.mul(new ComplexNoPattern(10,2)));
        assertNotEquals(new ComplexNoPattern(94520,7100),com.mul(new ComplexNoPattern(1012,278)));
    }

    @Test
    public void testeqauls() {
        assertEquals(new ComplexNoPattern(10,5),com);
        assertNotEquals(new ComplexNoPattern(5,10),com);
    }

    @Test
    public void testhash() {
        assertNotEquals(new ComplexNoPattern(346,23).hashCode(),com.hashCode());
        assertEquals(new ComplexNoPattern(10,5).hashCode(),com.hashCode());
        assertEquals(com.hashCode(),com.hashCode());
    }

}
