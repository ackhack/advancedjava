package A5;

import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ComplexTest {
    
    private static Complex com;

    @BeforeClass
    public static void setup() {
        com = Complex.valueOf(10,5);
    }

    @Test
    public void testneg() {
        assertEquals(Complex.valueOf(-10,-5), com.negate());
    }

    @Test
    public void testadd() {
        assertEquals(Complex.valueOf(20,7),com.add(Complex.valueOf(10,2)));
    }

    @Test
    public void testsub() {
        assertEquals(Complex.valueOf(0,3),com.sub(Complex.valueOf(10,2)));
        assertNotEquals(Complex.valueOf(10,3),com.sub(Complex.valueOf(1020,2450)));
    }

    @Test
    public void testmul() {
        assertEquals(Complex.valueOf(90,70),com.mul(Complex.valueOf(10,2)));
        assertNotEquals(Complex.valueOf(94520,7100),com.mul(Complex.valueOf(1012,278)));
    }

    @Test
    public void testeqauls() {
        assertEquals(Complex.valueOf(10,5),com);
        assertNotEquals(Complex.valueOf(5,10),com);
        assertSame(Complex.valueOf(10, 5), com);
    }

    @Test
    public void testhash() {
        assertNotEquals(Complex.valueOf(346,23).hashCode(),com.hashCode());
        assertEquals(Complex.valueOf(10,5).hashCode(),com.hashCode());
        assertEquals(com.hashCode(),com.hashCode());
    }
}
