package A3;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class Lamda_Streams {

    private static void printList(Iterable<Integer> source, Consumer<Integer> con) {
        for (Integer x : source) {
            con.accept(x);
        }
    }

    public static void evaluate(List<Integer> list, Predicate<Integer> predicate) {
        for (int x: list) {
            if (predicate.test(x)) {
                System.out.println(x);
            }
        }
    }

    public static void main(String[] args) {
        LinkedList<Integer> list = new LinkedList<>();
        for (int i = 1; i < 11; i++) {
            list.add(i);
        }
//        printList(list,System.out::println);
//        printList(list,x -> System.out.println(x));
//        list.stream().forEach(System.out::println);
//        evaluate(list,x -> x==x);
//        evaluate(list,x -> x!=x);
//        evaluate(list,x -> x%2 == 0);
//        evaluate(list,x -> x%2 == 1);
//        evaluate(list,x -> x>5);
//        list.stream().map(x -> x*x).forEach(System.out::println);
//        list.parallelStream().map(x -> x*x).forEach(System.out::println);
//        list.stream().filter(x -> x%2 == 0).map(x -> x*x).forEach(System.out::println);
        System.out.println(list.stream().filter(x -> x % 2 == 0).mapToInt(x -> x*x).sum());
    }
}