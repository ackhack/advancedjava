package A6;

import java.io.*;

public class html_builder {

    public static void main(String[] args) throws Exception {

        long firstTime = System.currentTimeMillis();
        StringBuilder sb = new StringBuilder();
        File file = new File("C:\\Users\\Chris\\IdeaProjects\\advancedjava\\src\\A6\\homepage.html");

        String head = "<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Homepage</title>\n" +
                "    <link href=\"homepage.css\" rel=\"stylesheet\">\n" +
                "    <script type=\"text/javascript\" src=\"homepage.js\"></script>\n" +
                "</head>\n" +
                "<body>\n";
        sb.append(head);

        for (int i = 0; i < 100000;i++) {
            sb.append("<div>\n");
            sb.append("<label>").append(i).append("</label>\n");
            sb.append("<label>").append(Math.sqrt(i)).append("</label>\n");
            sb.append("<label>").append(i * i).append("</label>\n");
            sb.append("<label>").append(Math.log(i)).append("</label>\n");
            sb.append("</div>\n");
        }

//        for (int i = 0; i < 10000;i++) {
//            head += "<div>\n";
//            head += "<label>" + i + "</label>\n";
//            head +="<label>" +  Math.sqrt(i) +  "</label>\n";
//            head += "<label>" +  i * i +  "</label>\n";
//            head += "<label>" +  Math.log(i) +  "</label>\n";
//            head += "</div>\n";
//        }

//        head += "</body>\n";
        sb.append("</body>\n");

        long lastTime = System.currentTimeMillis();
        lastTime -= firstTime;

//        System.out.println(head);
        FileWriter writer = new FileWriter(file);
        writer.write(String.valueOf(sb));
        writer.close();
        System.out.println("Time :" + lastTime + "ms");
    }
}
