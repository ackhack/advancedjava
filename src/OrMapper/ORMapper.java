package OrMapper;

import java.util.*;
import java.sql.*;

public class ORMapper {

    static Connection connection = null;

    ORMapper(Class<Student> studentClass){
        try {
            connection = DriverManager.getConnection("jdbc:derby://localhost:1527/jdbcStudentDB;create=true");
            //connection = DriverManager.getConnection("jdbc:derby://localhost:1527/jdbcStudentDB");
            if (!connection.isClosed()) {
                System.out.println("Successfully connected");
                createTable(connection);
            } else {
                throw new Exception("Not able to connect");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void createTable (Connection con) throws SQLException {
        Statement statement = con.createStatement();
        statement.executeUpdate("CREATE TABLE STUDENT (MATNR INTEGER,NAME VARCHAR(60) NOT NULL,PRIMARY KEY (MATNR))");
    }

    public void dropTable (Connection con) throws SQLException {
        Statement statement = con.createStatement();
        statement.executeUpdate("DROP TABLE STUDENT");
    }

    public void insert (Connection con, Object object) throws SQLException {
        PreparedStatement statement = con.prepareStatement("INSERT INTO student (MATNR, NAME) VALUES (?, ?)");
        Student student = (Student) object;
        statement.setInt(1,student.getMatrikelNummer());
        statement.setString(2,student.getName());
        statement.execute();
    }

    public List selectAll (Connection con) throws SQLException {
        Statement statement = con.createStatement();
        ResultSet resultSet = statement.executeQuery("SELECT MATNR, NAME FROM STUDENT");
        LinkedList<Student> list = new LinkedList<>();
        while (resultSet.next()) {
            list.add(new Student(resultSet.getInt("MATNR"),resultSet.getString("NAME")));
        }
        return list;
    }

    public static void main(String[] args) throws SQLException {
        ORMapper orMapper = null;
        try {
            orMapper = new ORMapper(Student.class);
            orMapper.insert(connection, new Student(20, "test20"));
            orMapper.insert(connection, new Student(40, "test40"));
            orMapper.insert(connection, new Student(42, "test42"));
            orMapper.insert(connection, new Student(80, "test80"));
            System.out.println(orMapper.selectAll(connection));
        } finally {
            assert orMapper != null;
            orMapper.dropTable(connection);
            connection.close();
        }

    }
}
