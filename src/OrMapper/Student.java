package OrMapper;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Array;
import java.util.Arrays;

@DbTable("STUDENT")
class Student {

    @DbColumn(name = "MATNR", type = "NUMBER(6)")
    @NotNull
    @Key(1)
    private int matrikelNummer;

    @DbColumn(name = "NAME", type = "VARCHAR(60)")
    @NotNull
    private String name;

    Student() {}

    Student(int matrikelNummer,String name) {
        this.matrikelNummer = matrikelNummer;
        this.name = name;
    }

    public void setMatrikelNummer(int matrikelNummer) {
        this.matrikelNummer = matrikelNummer;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMatrikelNummer() {
        return matrikelNummer;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Student{" +
                "matrikelNummer=" + matrikelNummer +
                ", name='" + name + '\'' +
                '}';
    }

    public static void printRef()  {
        Field[] fields = Student.class.getDeclaredFields();
        Arrays.stream(fields).forEach(System.out::println);

        for (Field f : fields) {
            if (f.toString().equals("private int OrMapper.Student.matrikelNummer")) {
                int x = 0;
                for(Annotation a: f.getAnnotations()) {
                    if (a.toString().equals("@OrMapper.DbColumn(name=\"MATNR\", type=\"NUMBER(6)\")") || a.toString().equals("@OrMapper.NotNull()") || a.toString().equals("@OrMapper.Key(value=1)")) {
                        x++;
                        if (a.toString().equals("@OrMapper.NotNull()")) {
                            System.out.println("Null is here");
                        }else {
                            System.out.println(a.toString());
                        }
                    }
                }
                System.out.println(x==3);
            }
            if (f.toString().equals("private java.lang.String OrMapper.Student.name")) {
                int x = 0;
                for(Annotation a: f.getAnnotations()) {
                    if (a.toString().equals("@OrMapper.DbColumn(name=\"NAME\", type=\"VARCHAR(60)\")") || a.toString().equals("@OrMapper.NotNull()")) {
                        x++;
                        if (a.toString().equals("@OrMapper.NotNull()")) {
                            System.out.println("Null is here");
                        }else {
                            System.out.println(a.toString());
                        }
                    }
                }
                System.out.println(x==2);
            }
        }
    }

    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        printRef();

        Student me = new Student();
        Method m1 = me.getClass().getMethod("setMatrikelNummer", int.class);
        m1.invoke(me,1);
        Method m2 = me.getClass().getMethod("setName", String.class);
        m2.invoke(me,"Christoph");
        System.out.println(me);

    }

}